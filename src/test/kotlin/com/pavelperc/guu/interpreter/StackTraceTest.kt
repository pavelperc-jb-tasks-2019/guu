package com.pavelperc.guu.interpreter

import com.pavelperc.guu.ast.CallStmt
import com.pavelperc.guu.ast.PrintStmt
import org.amshove.kluent.shouldEqual
import org.junit.Assert.*
import org.junit.Test

class StackTraceTest {
    
    @Test
    fun testFormatted() {
        
        val stackTrace = StackTrace(
            listOf(
                CallStmt(3, "hello"),
                CallStmt(5, "world")
            )
        )
        stackTrace.size shouldEqual 2
    
        stackTrace.getFormatted(PrintStmt(9, "a"))
            .shouldEqual("main:3\nhello:5\nworld:9")
        
    }
    
}