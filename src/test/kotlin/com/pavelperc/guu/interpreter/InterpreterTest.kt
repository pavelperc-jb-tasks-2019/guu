package com.pavelperc.guu.interpreter

import com.pavelperc.guu.debugger.Debugger
import com.pavelperc.guu.parser.parse
import org.amshove.kluent.shouldEqual
import org.junit.Assert.*
import org.junit.Test
import java.lang.StringBuilder
import kotlin.test.assertFailsWith

class InterpreterTest {
    
    fun interpretate(input: String, desiredOutput: String? = null, debugger: Debugger? = null) {
        val program = parse(input)
        val lines = mutableListOf<String>()
        
        val interpreter = Interpreter(program, debugger) { lines += it }
        interpreter.execute()
        
        if (desiredOutput != null) {
            desiredOutput shouldEqual lines.joinToString("\n")
        }
    }
    
    
    @Test
    fun simpleTest() {
        interpretate("""
            sub main
                set a 1
                print a
        """.trimIndent(), "a = 1")
    }
    
    @Test
    fun functions() {
        interpretate("""
            sub call_me
                set a 2
                set b 3
            sub main
                set a 1
                call call_me
                print a
                print b
        """.trimIndent(), "a = 2\nb = 3")
    }
    
    
    @Test
    fun recursion() {
        assertFailsWith<GuuStackOverflowError> {
            interpretate("""
            sub a
                call b
            sub b
                call main
            sub main
                call a
        """.trimIndent())
        }
    }
    
    
    @Test
    fun undefinedVar() {
        assertFailsWith<UndefinedVarError> {
            interpretate("""
            sub main
                print xxx
        """.trimIndent())
        }
    }
    
    
    @Test
    fun undefinedProc() {
        assertFailsWith<UndefinedProcError> {
            interpretate("""
            sub call_me
                print a
            sub main
                set a 10
                call callllll_me
        """.trimIndent())
        }
    }
    
    
    @Test
    fun noMainFun() {
        assertFailsWith<MainFunctionNotFoundError> {
            interpretate("""
            sub call_me
                print a
            sub main_fun
                set a 10
                call call_me
        """.trimIndent())
        }
    }
}