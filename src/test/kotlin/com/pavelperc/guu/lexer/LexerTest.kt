package com.pavelperc.guu.lexer

import com.pavelperc.guu.lexer.TokenType.*
import org.amshove.kluent.shouldBe
import org.amshove.kluent.shouldBeNull
import org.amshove.kluent.shouldEqual
import org.amshove.kluent.shouldNotBeNull
import org.junit.Test
import kotlin.test.assertFailsWith

class LexerTest {
    
    fun TokenType.shouldMatch(input: String, parsed: String? = null) {
        val token = this.match(input).shouldNotBeNull()
        token.type shouldBe this
        if (parsed != null) {
            token.value shouldEqual parsed
        }
    }
    
    fun TokenType.shouldNotMatch(input: String) {
        val token = this.match(input).shouldBeNull()
    }
    
    @Test
    fun testTokenTypes() {
        // test just several tokens. Others are similar.
        TokenType.CALL.shouldMatch("call xxxx", "call")
        TokenType.CALL.shouldMatch("call") // end of file
        TokenType.CALL.shouldMatch("call\n") // end of line
        TokenType.CALL.shouldNotMatch("call_me") // no whitespace after call
        TokenType.CALL.shouldNotMatch("")
        
        
        TokenType.ID.shouldMatch("_ABC__d hello", "_ABC__d")
        TokenType.ID.shouldMatch("call_123 hello", "call_123")
        TokenType.ID.shouldNotMatch("5abc")
        
        TokenType.NUMBER.shouldMatch("123", "123")
        TokenType.NUMBER.shouldMatch("-12 ", "-12")
        TokenType.NUMBER.shouldNotMatch("- 1")
        
        TokenType.EOL.shouldMatch("\n\n", "\n")
//        TokenType.SPACE.shouldMatch("   AAAA", "   ")
//        TokenType.SPACE.shouldMatch("\tAAAA", "\t")
    }
    
    @Test
    fun testEOF() {
        // EOF should not match anything, even an empty string!!
        TokenType.EOF.shouldNotMatch("abc\n")
        TokenType.EOF.shouldNotMatch("\n")
        TokenType.EOF.shouldNotMatch("")
    }
    
    @Test
    fun tokenizeTest() {
        tokenize("sub main\n\tcall  main")
            .map { it.type }
            .shouldEqual(listOf(SUB, ID, EOL, CALL, ID,  EOF))
        
        assertFailsWith<LexerError> { tokenize("#@!`/'") }
        
        tokenize("z_ 123").map { it.value } shouldEqual listOf("z_", "123", "")
    }
}