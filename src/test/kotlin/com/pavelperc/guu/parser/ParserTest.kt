package com.pavelperc.guu.parser

import com.pavelperc.guu.ast.*
import com.pavelperc.guu.lexer.TokenType.*
import com.pavelperc.guu.lexer.tokenize
import org.amshove.kluent.shouldEqual
import org.junit.Test
import kotlin.test.assertFailsWith

class ParserTest {
    
    
    @Test
    fun simpleTest() {
        val tokens = tokenize("hello world")
        tokens.map { it.type } shouldEqual listOf(ID, ID, EOF)
        
        Parser(tokens).apply {
            position shouldEqual 0
            lineNumber shouldEqual 1
            
            currToken.value shouldEqual "hello"
        }
    }
    
    @Test
    fun testMoving() {
        val tokens = tokenize("hello\nworld")
        tokens.map { it.type } shouldEqual listOf(ID, EOL, ID, EOF)
        
        Parser(tokens).apply {
            position shouldEqual 0
            
            move()
            position shouldEqual 1
            currToken.value shouldEqual "\n"
            
            parseEOL()
            position shouldEqual 2
            
            parseID()
            position shouldEqual 3
            
            move(10)
            assertFailsWith<EOFError> { currToken }
        }
    }
    
    @Test
    fun testRequireToken() {
        val tokens = tokenize("hello 123")
        tokens.map { it.type } shouldEqual listOf(ID, NUMBER, EOF)
        
        Parser(tokens).apply {
            position shouldEqual 0
            val token = requireToken(ID)
            token.type shouldEqual ID
            token.value shouldEqual "hello"
            position shouldEqual 1
            
            assertFailsWith<ParserError> {
                requireToken(CALL)
            }
            // rollback
            position shouldEqual 1
        }
    }
    
    @Test
    fun parseProgram1() {
        val program = parse(
            """
            sub main
                call call_me
            sub call_me
                set a 1
                
                print a
        """.trimIndent()
        )
        
        program shouldEqual Program(
            listOf(
                Procedure(
                    "main", 1, statements = listOf(CallStmt(2, "call_me"))
                ),
                Procedure(
                    "call_me", 3, statements = listOf(
                        SetStmt(4, "a", 1),
                        PrintStmt(6, "a")
                    )
                )
            )
        )
    }
    
    
    @Test
    fun parseProgram2() {
        assertFailsWith<ParserError> {
            parse(
                """
            sub main
            sub call_me
        """.trimIndent()
            ) // such code style is made by autoformatter (
        }
        
        assertFailsWith<ParserError> {
            parse(
                """
            sub main
                print print
        """.trimIndent()
            )
        }
        
        assertFailsWith<ParserError> {
            parse(
                """
            submain
                callmain
        """.trimIndent()
            )
        }
    }
}
