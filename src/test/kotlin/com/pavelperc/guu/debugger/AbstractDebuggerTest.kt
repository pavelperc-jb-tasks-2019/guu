package com.pavelperc.guu.debugger

import com.pavelperc.guu.ast.Statement
import com.pavelperc.guu.interpreter.Interpreter
import com.pavelperc.guu.parser.parse
import com.pavelperc.guu.debugger.DebuggerCommand.*
import org.amshove.kluent.shouldEqual
import org.amshove.kluent.shouldNotBeEmpty
import org.junit.Test
import java.util.*

class AbstractDebuggerTest {
    
    class TestDebugger(val commands: Queue<DebuggerCommand>) : AbstractDebugger() {
        
        val outputs = mutableListOf<String>()
        val lines = mutableListOf<Int>()
        
        override fun readCommand(stmt: Statement): DebuggerCommand {
            commands.shouldNotBeEmpty()
            lines += stmt.lineNumber
            return commands.poll()
        }
        
        override fun printOutput(output: String) {
            outputs += output
        }
    }
    
    
    fun debug(
        input: String,
        debuggerCommands: List<DebuggerCommand>,
        stepLines: List<Int>? = null,
        debuggerOutput: List<String>? = null
    ) {
        val program = parse(input)
        val debugger = TestDebugger(LinkedList(debuggerCommands))
        val interpreter = Interpreter(program, debugger)
        interpreter.execute()
        
        if (debuggerOutput != null) {
            debuggerOutput shouldEqual debugger.outputs
        }
        if (stepLines != null) {
            stepLines shouldEqual debugger.lines
        }
        
        
    }
    
    @Test
    fun simpleInput() {
        
        debug(
            """
        sub call_me
            set hello_world 10
            set bye 5
        
        sub main
            set hello_world 5
            call call_me
            
            print hello_world
            call call_me
            print bye
            
    """.trimIndent(),
            debuggerCommands = listOf(
                STEP_IN, // 6
                STEP_IN, // 7
                TRACE, // 2
                STEP_OUT, // 2
                VAR, // 9
                STEP_OVER, // 9
                STEP_OVER, // 10
                STEP_OVER // 11
            ),
            stepLines = listOf(6, 7, 2, 2, 9, 9, 10, 11),
            debuggerOutput = listOf("main:7\ncall_me:2", "hello_world = 10\nbye = 5")
        )
        
    }
}