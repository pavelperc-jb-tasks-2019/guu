package com.pavelperc.guu.lexer

import kotlin.math.min
import com.pavelperc.guu.lexer.TokenType.*
import com.pavelperc.guu.parser.ParserError

// Lexer:

data class Token(val type: TokenType, val value: String)

typealias TokenMatcher = TokenType.(CharSequence) -> Token?

class LexerError(msg: String) : ParserError(msg)

enum class TokenType(regexStr: String?) {
//    SPACE("""[ \t]+"""),
    SUB("""sub\b"""),
    SET("""set\b"""),
    CALL("""call\b"""),
    PRINT("""print\b"""),
    ID("""[A-Za-z_][A-Za-z_\d]*\b"""),
    NUMBER("""-?\d+"""),
    EOL("""\n"""),
    EOF(null);
    
    val regex = if (regexStr != null) Regex("($regexStr).*", RegexOption.DOT_MATCHES_ALL) else null
    
    fun match(input: CharSequence): Token? = regex
        ?.matchEntire(input)
        ?.groupValues
        ?.get(1)
        ?.let { matched -> Token(this, matched) }
}

fun tokenize(input: String): List<Token> {
    val tokens = mutableListOf<Token>()
    
    val tokenTypes = TokenType.values()
    
    var position = 0
    val spaces = setOf(' ', '\t', '\r')
    while (position < input.length) {
        if (input[position] in spaces) {
            position++
            continue
        }
        
        val token = tokenTypes
            .asSequence()
            .map { tokenType ->
                tokenType.match(input.subSequence(position, input.length))
            }
            .firstOrNull { it != null }
            ?: throw LexerError("Unknown token: ${input.substring(position, min(position + 20, input.length))}...")
        
        position += token.value.length
        tokens += token
    }
    tokens += Token(EOF, "")
    
    return tokens
}