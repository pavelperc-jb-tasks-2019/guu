package com.pavelperc.guu.interpreter

import com.pavelperc.guu.ast.*
import com.pavelperc.guu.debugger.Debugger
import java.io.PrintStream
import java.lang.StringBuilder

open class InterpreterError(msg: String) : IllegalStateException(msg)
class GuuStackOverflowError(stackTrace: String) :
    InterpreterError("StackOverflow: Max stack size $MAX_STACK_SIZE was reached. stacktrace:\n$stackTrace")

class UndefinedProcError(stmt: CallStmt) :
    InterpreterError("Undefined procedure ${stmt.procName} at line ${stmt.lineNumber}")

class UndefinedVarError(stmt: PrintStmt) :
    InterpreterError("Undefined variable ${stmt.varName} at line ${stmt.lineNumber}")


class MainFunctionNotFoundError() :
    InterpreterError("Main function not found.")


class StackTrace(list: List<CallStmt> = listOf()) : List<CallStmt> by list {
    /**
     * stacktrace all previous callStmt.
     * But we output in format `method:lineOfStmt`
     *
     * @param stmt the last (not only call) statement of stack trace. */
    fun getFormatted(stmt: Statement): String {
        
        val sb = StringBuilder("main:")
        
        this.forEach {
            sb.append("${it.lineNumber}\n")
            
            sb.append("${it.procName}:")
        }
        sb.append(stmt.lineNumber)
        
        return sb.toString()
    }
}

const val MAX_STACK_SIZE = 20

class Interpreter(
    val program: Program,
    val degugger: Debugger? = null,
    val linePrinter: (String) -> Unit = { println(it) }
) {
    val procNameToProc = program.procedures.map { it.name to it }.toMap()
    
    val definedVars = mutableMapOf<String, Int>()
    
    /** [stackTrace] contains all [CallStmt] before entering this procedure. */
    fun Procedure.execute(stackTrace: StackTrace) {
        if (stackTrace.size > MAX_STACK_SIZE) {
            // use the previous stack trace and last call as final stmt.
            throw GuuStackOverflowError(StackTrace(stackTrace.dropLast(1)).getFormatted(stackTrace.last()))
        }
        
        statements.forEach { stmt ->
            degugger?.debug(stmt, stackTrace, this@Interpreter)
            
            when (stmt) {
                is SetStmt -> definedVars[stmt.varName] = stmt.value
                is CallStmt -> {
                    val nextProc = procNameToProc[stmt.procName]
                        ?: throw UndefinedProcError(stmt)
                    
                    // intentionally don't make stackTrace mutable, to avoid side effects
                    nextProc.execute(StackTrace(stackTrace + stmt))
                }
                is PrintStmt -> {
                    val value = definedVars[stmt.varName]
                        ?: throw UndefinedVarError(stmt)
                    
                    linePrinter("${stmt.varName} = $value")
                }
            }
        }
    }
    
    fun execute() {
        val main = procNameToProc["main"] ?: throw MainFunctionNotFoundError()
        main.execute(StackTrace())
        
    }
}