package com.pavelperc.guu.debugger

import com.pavelperc.guu.ast.Statement
import com.pavelperc.guu.interpreter.Interpreter
import com.pavelperc.guu.interpreter.StackTrace

import com.pavelperc.guu.debugger.DebuggerCommand.*

interface Debugger {
    fun debug(stmt: Statement, stackTrace: StackTrace, interpreter: Interpreter)
}

enum class DebuggerCommand {
    STEP_IN, STEP_OVER, STEP_OUT, TRACE, VAR
}

abstract class AbstractDebugger() : Debugger {

    var keepStackTraceOfSize = 0
    var stepOver = false
    var stepOut = false
    
    abstract fun readCommand(stmt: Statement): DebuggerCommand

    /** print debugger output. */
    abstract fun printOutput(output: String)

    override fun debug(stmt: Statement, stackTrace: StackTrace, interpreter: Interpreter) {
        if (stepOut && stackTrace.size >= keepStackTraceOfSize) {
            return
        }
        if (stepOver && stackTrace.size > keepStackTraceOfSize) {
            return
        }
        stepOver = false
        stepOut = false

        inputLoop@ while (true) {
            val command = readCommand(stmt)

            when (command) {
                STEP_IN -> break@inputLoop
                STEP_OVER -> {
                    stepOver = true
                    keepStackTraceOfSize = stackTrace.size
                    
                    break@inputLoop
                }
                STEP_OUT -> {
                    stepOut = true
                    keepStackTraceOfSize = stackTrace.size
        
                    break@inputLoop
                }
                TRACE -> {
                    printOutput(stackTrace.getFormatted(stmt))
                }
                VAR -> {
                    val output = interpreter.definedVars.toList().let { vars ->
                        if (vars.isNotEmpty()) {
                            vars.joinToString("\n") { (name, value) -> "$name = $value" }
                        } else {
                            "No Vars."
                        }
                    }
                    printOutput(output)
                }
            }
            continue@inputLoop
        }
    }
}

class ConsoleDebugger() : AbstractDebugger() {

    override fun readCommand(stmt: Statement): DebuggerCommand {
        while (true) {
            println("${stmt.lineNumber}: ---> $stmt")

            when (readLine()!!) {
                "in" -> return STEP_IN
                "over" -> return STEP_OVER
                "out" -> return STEP_OUT
                "trace" -> return TRACE
                "var" -> return VAR
                else -> println("Unknown command. You can type in, over, out, trace, var.")
            }
        }
    }

    /** print debugger output. */
    override fun printOutput(output: String) {
        println(output)
    }
}