package com.pavelperc.guu.parser

import com.pavelperc.guu.ast.*
import com.pavelperc.guu.lexer.Token
import com.pavelperc.guu.lexer.TokenType
import com.pavelperc.guu.lexer.TokenType.*
import com.pavelperc.guu.lexer.tokenize

// Parser:

open class ParserError(msg: String) : IllegalStateException(msg)
class EOFError(msg: String) : ParserError(msg)


class Parser(val tokens: List<Token>) {
    
    var position: Int = 0
    var lineNumber: Int = 1
    
    /** Returns current Token, but doesn't change the [position]. */
    val currToken: Token
        get() = tokens.getOrNull(position) ?: throw EOFError("No More Tokens.")
    
    /** Increase the position. */
    fun move(step: Int = 1) {
        position += step
    }
    
    fun requireToken(type: TokenType): Token {
        val token = currToken
        if (token.type == type) {
            move()
            return token
        } else {
            throw ParserError(
                "Bad token: got $token instead of $type at line $lineNumber."
            )
        }
    }
    
    fun parseID() = requireToken(ID).value
    fun parseNumber() = requireToken(NUMBER).value.toInt()
    fun parseEOL() = requireToken(EOL)
    
    fun parseProgram(): Program {
        var procBuilder = Procedure.Builder()
        var firstProc = true
        
        val procedures = mutableListOf<Procedure>()
        
        while (currToken.type != EOF) {
            val token = currToken
            move()
            when (token.type) {
                SUB -> {
                    if (firstProc) {
                        firstProc = false
                    } else {
                        // build and append previous procedure
                        procedures += procBuilder.build()
                        procBuilder = Procedure.Builder()
                    }
                    procBuilder.name = parseID()
                    procBuilder.lineNumber = lineNumber
                }
                SET -> {
                    val id = parseID()
                    val number = parseNumber()
                    procBuilder.statements += SetStmt(lineNumber, id, number)
                }
                CALL -> {
                    procBuilder.statements += CallStmt(lineNumber, parseID())
                }
                PRINT -> {
                    procBuilder.statements += PrintStmt(lineNumber, parseID())
                }
                EOL -> {
                    lineNumber++
                }
                else -> throw ParserError("Unexpected token: $token, line $lineNumber")
            }
        }
        // add the last
        procedures += procBuilder.build()
        
        procedures.checkNameCollisions()
        return Program(procedures)
    }
    
    private fun List<Procedure>.checkNameCollisions() {
        val duplicates = this.groupBy { it.name }.filter { it.value.size > 1 }.keys
        
        if (duplicates.isNotEmpty()) {
            throw ParserError("Found duplicate procedure names: $duplicates.")
        }
    }
    
}

fun parse(text: String) = Parser(tokenize(text)).parseProgram()
