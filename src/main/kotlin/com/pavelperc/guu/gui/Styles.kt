package com.pavelperc.guu.gui

import javafx.scene.paint.Color
import javafx.scene.text.FontWeight
import tornadofx.Stylesheet
import tornadofx.c
import tornadofx.cssclass

class Styles : Stylesheet() {
    companion object {
        val markedLineNum by cssclass()
        val unmarkedLineNum by cssclass()
        val codeArea by cssclass()
    }
    
    init {
        markedLineNum {
            backgroundColor += Color.ORANGE
        }
        unmarkedLineNum {
            backgroundColor += c("93e33655")
//            backgroundColor += Color.TRANSPARENT
        }
        
        codeArea {
            ".paragraph-box" {
                and(":has-caret") {
                    backgroundColor += Color.LIGHTBLUE.brighter()
                }
            }
            
            ".keyword" {
                fontWeight = FontWeight.BOLD
            }
            
            ".variable" {
                fill = Color.BLUE
            }
            
            ".number" {
                fill = Color.GREEN
            }
        }
        
    }
}