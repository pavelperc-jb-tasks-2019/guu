package com.pavelperc.guu.gui

import com.pavelperc.guu.debugger.DebuggerCommand
import com.pavelperc.guu.lexer.TokenType
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.Button
import javafx.scene.control.TextArea
import javafx.scene.image.Image
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import javafx.stage.Stage
import org.fxmisc.richtext.CodeArea
import org.fxmisc.richtext.model.StyleSpans
import org.fxmisc.richtext.model.StyleSpansBuilder
import org.reactfx.value.Val
import tornadofx.*
import java.time.Duration
import java.util.function.IntFunction


class MyApp : App(MainView::class, Styles::class) {
    override fun start(stage: Stage) {
        stage.icons += Image("icon.png")
        super.start(stage)
    }
}

fun main(args: Array<String>) {
    launch<MyApp>(args)
}

class MainView : View("GUUUUU Debugger") {
    val controller by inject<UIController>()
    
    override val root = vbox {
        hgrow = Priority.ALWAYS
        useMaxSize = true
        
        // code area
        
        val codeArea = CodeArea()
        codeArea.attachTo(this) {
            useMaxSize = true
            vgrow = Priority.ALWAYS
            setPrefSize(500.0, 300.0)
            addClass(Styles.codeArea)
            
            editableProperty().bind(controller.isDebugging.not())
            
            // replace tabs with spaces!!!
            setOnKeyTyped { event ->
                if (event.character == "\t") {
                    replaceText(caretPosition - 1, caretPosition, "    ")
                }
            }
    
    
            // recompute the syntax highlighting 500 ms after user stops editing area
            codeArea
                .multiPlainChanges()
                .successionEnds(Duration.ofMillis(500))
                .subscribe { codeArea.setStyleSpans(0, computeHighlighting(codeArea.text)) }
            
            
            // setup initial text
            appendText(controller.programText.value)
            controller.programText.bind(textProperty())
            
            // setup line numbers:
            paragraphGraphicFactory = IntFunction {
                // start lines from 1
                val lineNumber = it + 1
//                println("new line number $lineNumber")
                HBox().apply {
                    paddingRight = 5.0
                    
                    label("$lineNumber") {
                        minWidth = 30.0
                        paddingLeft = 5.0
                        
                        // do this to avoid memory leaks!!!!!
                        val highlighted = Val.map(
                            controller.debuggedLine
                        ) { sl -> sl == lineNumber }
                        
                        addClass(Styles.unmarkedLineNum)
                        
                        highlighted.onChange { highlight ->
                            highlight!!
                            toggleClass(Styles.markedLineNum, highlight)
                            toggleClass(Styles.unmarkedLineNum, !highlight)
                        }
                    }
                }
            }
        }
        
        val outputArea = TextArea()
        val debugArea = TextArea()
        
        fun Button.hintShortcut(shortcut: String) {
            shortcut(shortcut)
            tooltip(shortcut)
        }
        
        // buttons
        
        hbox {
//            spacing = 10.0
            paddingLeft = 10.0
            paddingRight = 10.0
//            paddingTop = 10.0
            button("run") {
                hintShortcut("Shift+f10")
                enableWhen(controller.isDebugging.not())
                action {
                    controller.run(outputArea)
                }
            }
            button("debug") {
                hintShortcut("Ctrl+Shift+f10")
                enableWhen(controller.isDebugging.not())
                action {
                    controller.debug(outputArea, debugArea)
                }
            }
            hbox {
//                spacing = 10.0
                removeWhen(controller.isDebugging.not())
                button("stop") {
                    hintShortcut("Ctrl+f2")
                    action {
                        controller.cancelDebug()
                    }
                }
                button("in") {
                    hintShortcut("f7")
                    action {
                        controller.debugCommandQueue.put(DebuggerCommand.STEP_IN)
                    }
                }
                button("over") {
                    hintShortcut("f8")
                    action {
                        controller.debugCommandQueue.put(DebuggerCommand.STEP_OVER)
                    }
                }
                button("out") {
                    hintShortcut("Shift+f8")
                    action {
                        controller.debugCommandQueue.put(DebuggerCommand.STEP_OUT)
                    }
                }
                button("defiined vars") {
                    hintShortcut("v")
                    action {
                        controller.debugCommandQueue.put(DebuggerCommand.VAR)
                    }
                }
                button("stack trace") {
                    hintShortcut("t")
                    action {
                        controller.debugCommandQueue.put(DebuggerCommand.TRACE)
                    }
                }
            }
        }
        
        
        hbox {
            hgrow = Priority.ALWAYS
            useMaxSize = true
            // output
            vbox {
                hgrow = Priority.ALWAYS
                useMaxSize = true
                label("output:") {
                    paddingLeft = 10.0
                }
                outputArea.attachTo(this) {
                    isEditable = false
                    hgrow = Priority.ALWAYS
                    useMaxSize = true
    
                    prefHeight = 120.0
                    minHeight = 120.0
                }
            }
            // debug output
            vbox {
                hgrow = Priority.ALWAYS
                useMaxSize = true
                removeWhen(controller.isDebugging.not())
                
                hbox {
                    paddingLeft = 10.0
                    paddingRight = 10.0
                    label("debug output:")
                    hbox {
                        alignment = Pos.CENTER_RIGHT
                        useMaxWidth = true
                        hgrow = Priority.ALWAYS
                        label("position: ")
                        label(controller.debuggedLine)
                    }
                    
                }
                
                debugArea.attachTo(this) {
                    hgrow = Priority.ALWAYS
                    useMaxSize = true
                    
                    isEditable = false
                    prefHeight = 120.0
                    minHeight = 120.0
                }
            }
            
            
        }
        
    }
    
    private fun computeHighlighting(text: String): StyleSpans<Collection<String>>? {
        var position = 0
        var lastSpan = 0
        
        val spansBuilder = StyleSpansBuilder<Collection<String>>()
        
        fun addSpan(type: String) {
            spansBuilder.add(listOf(type), position - lastSpan)
            lastSpan = position
        }
        fun emptySpan() {
            spansBuilder.add(emptyList(), position - lastSpan)
            lastSpan = position
        }
        
        
        
        while (position < text.length) {
            
            val subSequence = text.subSequence(position, text.length)
            
            val keywordTokens = listOf(TokenType.PRINT, TokenType.SET, TokenType.CALL, TokenType.SUB)
            
            val matchedKeyword = keywordTokens.mapNotNull { it.match(subSequence) }.firstOrNull()
            val matchedId = TokenType.ID.match(subSequence)
            val matchedNumber = TokenType.NUMBER.match(subSequence)
            
            if (matchedKeyword != null) {
                emptySpan()
                position += matchedKeyword.value.length
                addSpan("keyword")
            } else if (matchedId != null) {
                emptySpan()
                position += matchedId.value.length
                addSpan("variable")
            } else if (matchedNumber != null) {
                emptySpan()
                position += matchedNumber.value.length
                addSpan("number")
            } else {
                position++
            }
        }
        emptySpan()
        return spansBuilder.create()
    }
}
