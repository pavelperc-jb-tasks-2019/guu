package com.pavelperc.guu.gui

import com.pavelperc.guu.ast.Statement
import com.pavelperc.guu.debugger.AbstractDebugger
import com.pavelperc.guu.debugger.Debugger
import com.pavelperc.guu.debugger.DebuggerCommand
import com.pavelperc.guu.interpreter.Interpreter
import com.pavelperc.guu.interpreter.InterpreterError
import com.pavelperc.guu.parser.ParserError
import com.pavelperc.guu.parser.parse
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleStringProperty
import javafx.concurrent.Task
import javafx.scene.control.TextInputControl
import tornadofx.Controller
import tornadofx.error
import tornadofx.runLater
import java.util.concurrent.LinkedBlockingDeque

class UIController() : Controller() {
    inner class UIDebugger(val outputArea: TextInputControl) : AbstractDebugger() {
        override fun printOutput(output: String) {
            
            // on ui thread
            runLater {
                outputArea.clear()
                outputArea.appendText(output)
            }
            
        }
        
        override fun readCommand(stmt: Statement): DebuggerCommand {
            runLater {
                debuggedLine.value = stmt.lineNumber
                
            }
            
            val command = debugCommandQueue.take()
            return command
        }
        
    }
    
    val debugCommandQueue = LinkedBlockingDeque<DebuggerCommand>()
    
    
    val isDebugging = SimpleBooleanProperty(false)
    
    
    val programText = SimpleStringProperty(
        """
        sub call_me
            set hello_world 10
            set bye 5
        
        sub main
            set hello_world 5
            call call_me
            
            print hello_world
            call call_me
            print bye
            
    """.trimIndent()
    )
    
    val debuggedLine = SimpleIntegerProperty(0)
    
    private var executeTask: Task<Unit>? = null
    
    fun cancelDebug() {
        executeTask?.cancel()
    }
    
    /** Run with error callback. */
    private fun execute(
        input: String,
        outputArea: TextInputControl,
        debugger: Debugger? = null,
        onFinish: () -> Unit = { }
    ) {
        outputArea.clear()
        
        executeTask = runAsync(daemon = true) {
            val program = parse(input)
            val interpreter = Interpreter(program, debugger) { output ->
                runLater {
                    outputArea.appendText("$output\n")
                }
            }
            interpreter.execute()
            
        }.apply {
            setOnCancelled {
                println("Canceled!")
                onFinish()
            }
            setOnSucceeded {
                println("Succeeded!")
                onFinish()
            }
            setOnFailed {
                println("Failed!")
                
                val e = exception
                when (e) {
                    is ParserError -> error("Compilation error", e.message)
                    is InterpreterError -> error("Runtime error", e.message)
                    else -> throw e
                }
                
                onFinish()
            }
            
        }
    }
    
    fun run(outputArea: TextInputControl) {
//        execute(programText.value, outputArea)
//        return
        // duplicate code!!! but debug crashes sometimes.
        
        outputArea.clear()
        
        try {
            val program = parse(programText.value)
            val interpreter = Interpreter(program, null) { outputArea.appendText("$it\n") }
            interpreter.execute()
            
        } catch (e: ParserError) {
            error("Compilation error", e.message)
        } catch (e: InterpreterError) {
            error("Runtime error", e.message)
        }
        
    }
    
    fun debug(outputArea: TextInputControl, debugOutputArea: TextInputControl) {
        debugCommandQueue.clear()
        isDebugging.value = true
        debugOutputArea.clear()
        
        val debugger = UIDebugger(debugOutputArea)
        
        execute(programText.value, outputArea, debugger, onFinish = {
            isDebugging.value = false
            debuggedLine.value = -1
        })
    }
}