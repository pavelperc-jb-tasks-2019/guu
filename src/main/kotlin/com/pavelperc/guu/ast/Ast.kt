package com.pavelperc.guu.ast

import com.pavelperc.guu.parser.ParserError

sealed class Statement {
    abstract val lineNumber: Int
}

data class SetStmt(override val lineNumber: Int, val varName: String, val value: Int) : Statement() {
    override fun toString() = "set $varName = $value"
}
data class CallStmt(override val lineNumber: Int, val procName: String) : Statement() {
    override fun toString() = "call $procName"
}
data class PrintStmt(override val lineNumber: Int, val varName: String) : Statement() {
    override fun toString() = "print $varName"
}

data class Procedure(val name: String, val lineNumber: Int, val statements: List<Statement>) {
    class Builder {
        var name: String? = null
        var lineNumber: Int = -1
        val statements = mutableListOf<Statement>()

        private fun require(condition: Boolean, lazyMsg: () -> String) {
            if (!condition) {
                throw ParserError(lazyMsg())
            }
        }

        fun build(): Procedure {
            require(name != null) { "Procedure name is null." }
            require(statements.isNotEmpty()) { "Procedure $name is empty." }
            require(lineNumber > 0) { "Procedure $name line number should be > 0." }
            return Procedure(name!!, lineNumber, statements)
        }
    }
}

data class Program(val procedures: List<Procedure>)
