package com.pavelperc.guu

import com.pavelperc.guu.debugger.ConsoleDebugger
import com.pavelperc.guu.interpreter.Interpreter
import com.pavelperc.guu.lexer.tokenize
import com.pavelperc.guu.parser.Parser
import java.io.File


fun main(args: Array<String>) {
    val filePath = if (args.isEmpty())
        "program.guu"
    else args[0]

    val text = File(filePath).readText()
    println("loaded file $filePath")
    
//    val text = """
//        sub main
//            set a 1
//            print a
//            call foo
//            print a
//            
//            call main
//        
//        sub foo
//            set a 2
//            set b 5
//            print b
//    """.trimIndent()

    val tokens = tokenize(text)
//    println(tokens.map { it.value })
    val program = Parser(tokens).parseProgram()

    val debugger = ConsoleDebugger()

    val interpreter = Interpreter(program, debugger)
    
    println("You can enter the following commands:\n" +
            "in(step in), over(step over), out(step out), trace(print stack trace), var(defined vars).")
    interpreter.execute()
}